;
(function(window,undefined) {
     CarouselType1 = function() {

          var
          carousel = {
               currentStep : null,
               steps : null,
               sizeItemText : null
          },
          config = {
               autoplay : true,
               duration : 1000,
               typeFx : Expo
          },
          settings, $reference, $referencePosters, $referenceNav, $referenceTexts, isChanging = false;

          //actions
          //=======
          var actions = function() {
               $referenceNav.on("click", "li a", function(){
                         if(!isChanging) goTo($(this).parent().index());
               })
          }

          //animate the poster
          //==================
          var animationPoster = function(_index) {
               isChanging = true;
               var
               $posters = $referencePosters.find('img'),
                    $activePoster = $posters.eq(_index);
               listen(_index);
               $activePoster.addClass('animated').css('opacity',1);
               //fadeout
               $referencePosters.find('.active').animate({'opacity':0},{duration:settings.duration,complete:function() {
                         $(this).removeClass('active');
                         $activePoster.addClass('active').css('opacity',1);
                         $posters.removeClass('animated');
                         isChanging = false;
               }});
          }

          var animationText = function(_index) {
               var
               $texts = $referenceTexts.find('.item'),
                    $activeText = $texts.eq(_index);
               $activeText.css('left',carousel.sizeItemText*-1);
               //fadeout
               $referenceTexts.find('.active').animate({'left':carousel.sizeItemText*-1},{ easing:settings.typeFx.easeIn, duration:(settings.duration/2),complete:function() {
                         $(this).removeClass('active');
                         $activeText.addClass('active').animate({'left':0},{easing:settings.typeFx.easeOut, duration:(settings.duration/2)});
               }});
          }

          //go to step
          //==========
          var goTo = function(step,fn) {
               var goToStep;
               switch(step) {
                    default:
                         carousel.currentStep = step;
                         break
               }
               animationPoster(carousel.currentStep);
               animationText(carousel.currentStep);
          }

          //initialize the module
          //=====================
          var initialize = function(_reference,_options) {
               //settings
               settings = $.extend({},config,_options);
               $reference = $(_reference);
               $referencePosters = $reference.find('.slideshow-posters');
               $referenceTexts = $reference.find('.slideshow-items');
               $referenceNav = $reference.find('.slideshow-trigger ul');
               carousel.currentStep = RequestManager.getDataAttribute(_reference,'data-active');
               carousel.steps = $referencePosters.length;
               carousel.sizeItemText = $referenceTexts.find('.active').outerWidth(true);
               //actions
               actions();
               return this;
          }

          //listen carousel
          //===============
          var listen = function(_index) {
               $referenceNav.find('li').removeClass('active');
               $referenceNav.find('li').eq(_index).addClass('active');
          }

          return {
               initialize : initialize
          }
     }()//ready to use
     //expose for window
     window.CarouselType1 = CarouselType1;
})(window);