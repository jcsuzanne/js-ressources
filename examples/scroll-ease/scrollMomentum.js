/*!
 * scrollMomentum.js
 * @author Jean-Christophe Suzanne <jc.suzanne@gmail.com>
 * @license New BSD License <http://creativecommons.org/licenses/BSD/>

 Usage
 -----
 var gScroll = scrollmomentum.init({
    body : $('#master'),
    receptor : $('#seReceptor'),
    fx : {
        duration : 1,
        easing : Power2.easeOut
    }
});
 */
(function(window, document, undefined) {
'use strict';

    var scrollmomentum = window.scrollmomentum = {
        get: function() {
            return _instance;
        },
        //Main entry point.
        init: function(options) {
            return _instance || new scrollMomentum(options);
        }
    };

    /**
     * Constructor.
     */
    function scrollMomentum(options) {
        _instance       =   this;
        settings        =   $.extend({},defaults,options);
        $body           =   settings.body;
        $receptor       =   settings.receptor
        _instance.construct();
        return _instance;
    }

    scrollMomentum.prototype.construct = function()
    {
        _instance.setDimensions();
        _instance.scroll();

    }

    scrollMomentum.prototype.setDimensions = function() {
        $body.css({
            'height':$receptor.height()
        });
    }

    scrollMomentum.prototype.scroll = function() {
        $window.on('scroll',function(e) {
            var
                getY    =   $(this).scrollTop()
            ;
            if(typeof tween != 'undefined') TweenLite.killTweensOf($receptor);
            tween = TweenLite.to(
                $receptor,
                settings.fx.duration,
                {
                    y:-(getY),
                    force3D : true,
                    ease:settings.fx.easing
                }
            );
        });
    }

    // Singleton
    var
        _instance
    ,   $body
    ,   $receptor
    ,   $window         =   $(window)
    ,   tween
    ,   defaults        =   {
            fx : {
                duration : .5,
                easing : Power3.easeOut
            }
        }
    ,   settings        =   {}
    ;
}(window, document));