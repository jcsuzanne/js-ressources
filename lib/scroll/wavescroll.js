/*!
 * waveScroll.js
 * @author Jean-Christophe Suzanne <jc.suzanne@gmail.com>
 * @license New BSD License <http://creativecommons.org/licenses/BSD/>

 Usage
 -----
     var wave = wavescroll.init({
        elements : items of the list to anim
        receptor : element to listen to get the scrolltop position
        allowBgMove : allow background moving
    });
});

Dependencies
------
jquery + tweenmax.js (if scrollmomentum is used)

Instance
------
http://dev.jcsuzanne.com/proto-scroll/
 */
(function(window, document, undefined) {
'use strict';

    var wavescroll = window.wavescroll = {
        get: function() {
            return _instance;
        },
        //Main entry point.
        init: function(options) {
            return _instance || new waveScroll(options);
        }
    };

    function waveScroll(options) {
        _instance       =   this;
        settings        =   $.extend({},defaults,options);
        if(settings.elements.length) {
            $items          =   settings.elements;
            $receptor       =   settings.receptor;
            if(settings.allowBgMove) {
                var
                    $firstItem  =   settings.elements.eq(0)
                ;
                bgHeightGap     =   $firstItem.find('.bg').height() - $firstItem.height();
            }
            _instance.construct();
        }
        else
        {
            throw 'wavescroll has not elements to match';
        }

        return _instance;
    }

    waveScroll.prototype.construct = function()
    {
        _instance.build();
        _instance.loop();
    }

    waveScroll.prototype.build = function()
    {
        $items.data('locked',false);
        $items.each(function() {
            var
                $ref        =   $(this)
            ;
            $ref.data('offsettop',$ref[0].offsetTop);
            $ref.data('offsetbottom',$ref[0].offsetTop + $ref.height());
            $ref.data('state','');
        });
    }

    waveScroll.prototype.loop = function() {
        _instance.update();
        request = requestAnimFrame(_instance.loop);
    }

    waveScroll.prototype.update = function() {
        if(typeof $receptor[0]._gsTransform != 'undefined') {
            scrollPosTop        =   Math.abs(parseInt($receptor[0]._gsTransform.y));
            scrollPosBottom     =   scrollPosTop + $(window).height();
        }
        $items.each(function(i) {
            var
                $ref    =   $(this)
            ;
            // move bg
            if(settings.allowBgMove) {
                var
                    bgMoveY     =   (scrollPosTop/$ref.data('offsetbottom')*bgHeightGap)*-1
                ;
                TweenLite.set($ref.find('.bg'),{ y : bgMoveY , force3D : true});
            }
            //Above list viewport
            if( $ref.data('offsetbottom')<scrollPosTop ) {
                if( $ref.data('state') !== 'past' ) {
                    $ref.data('state','past');
                    $ref.addClass( 'past' );
                    $ref.removeClass( 'future' );
                }
            }
            // Below list viewport
            else if( $ref.data('offsettop') > scrollPosBottom ) {
                if( $ref.data('state') !== 'future' ) {
                    $ref.data('state','future');
                    $ref.addClass( 'future' );
                    $ref.removeClass( 'past' );
                }
            }
            // Inside of list viewport
            else if( $ref.data('state') ) {
                if( $ref.data('state') === 'past' ) $ref.removeClass( 'past' );
                if( $ref.data('state') === 'future' ) $ref.removeClass( 'future' );
                $ref.data('state','');
            }
        });
    }

    // Singleton
    var
        _instance
    ,   defaults        =   {
            allowBgMove : false,
            elements : ''
        }
    ,   settings        =   {}
    ,   bgHeightGap     =   0
    ,   request
    ,   $items
    ,   $receptor
    ,   scrollPosTop
    ,   scrollPosBottom
    ;
}(window, document));