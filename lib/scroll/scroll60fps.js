;(function(window,undefined) {
    scroll60Fps = function() {

        //init the module
        //===============
        var init = function(_sources,_fnReady,_callback) {
            var
                body = document.body
            ,   timer
            ;
            window.addEventListener('scroll', function() {
            clearTimeout(timer);
            if(typeof body.classList != 'undefined') {
                if(!body.classList.contains('disable-hover')) {
                    body.classList.add('disable-hover')
                }
            } else {
                if(body.className.indexOf('disable-hover') == -1)
                {
                    body.className = 'disable-hover';
                }
            }

            timer = setTimeout(function(){
                if(typeof body.classList != 'undefined') {
                    body.classList.remove('disable-hover');
                } else {
                    body.className = '';
                }
            },500);
            }, false);
        }

          //public methods
        return {
            'init':init
        }
    }()
    window.scroll60Fps = scroll60Fps;
})(window);