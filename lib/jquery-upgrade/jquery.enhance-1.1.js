/*
 * Returning Scroll dimensions
 * http://api.jquery.com/outerHeight/
 *
 */
jQuery.fn.outerScrollWidth=function(includeMargin){var element=this[0];var jElement=$(element);var totalWidth=element.scrollWidth;totalWidth+=jElement.outerWidth(includeMargin)-jElement.innerWidth();return totalWidth};
jQuery.fn.outerScrollHeight=function(includeMargin){var element=this[0];var jElement=$(element);var totalHeight=element.scrollHeight;totalHeight+=jElement.outerHeight(includeMargin)-jElement.innerHeight();return totalHeight};

/**
 * http://stackoverflow.com/questions/1184624/convert-form-data-to-js-object-with-jquery
 * BOWER!
 */
$.fn.serializeObject = function()
{
   var o = {};
   var a = this.serializeArray();
   $.each(a, function() {
       if (o[this.name]) {
           if (!o[this.name].push) {
               o[this.name] = [o[this.name]];
           }
           o[this.name].push(this.value || '');
       } else {
           o[this.name] = this.value || '';
       }
   });
   return o;
};

/**
* https://gist.github.com/oomlaut/1101534
*/
jQuery.extend({
    parseQuerystring: function() {
        var nvpair = {};
        var qs = window.location.search.replace('?', '');
        var pairs = qs.split('&');
        $.each(pairs, function(i, v) {
            var pair = v.split('=');
            nvpair[pair[0]] = pair[1];
        });
        return nvpair;
    }
});


/*! Copyright (c) 2009 Brandon Aaron (http://brandonaaron.net)
* Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
* and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
*
* Version: 1.0
*/

(function($) {
    $.event.special.mousemove = {
        setup: function(data, namespaces) {
            var elem = this,
            behavior = data && data.behavior || "throttle", // "throttle" or "debounce"
            delay = data && data.delay || 100,
            handler = delayedHandler.call(elem, actualHandler, behavior, delay);
            $.data(elem, 'delayedmousemovehandler', handler);
            if ( elem.addEventListener )
                elem.addEventListener('mousemove', handler, false);
            else if ( elem.attachEvent )
                elem.attachEvent('onmousemove', handler);
        },

        teardown: function(data, namespaces) {
            var elem = this,
            handler = $.data(elem, 'delayedmousemovehandler');
            if ( elem.removeEventListener )
                elem.removeEventListener('mousemove', handler, false);
            else if ( elem.detachEvent )
                elem.detachEvent('onmousemove', handler);
        }
    };

    function delayedHandler(fn, behavior, delay) {
        var timeout, scope = this;
        return function() {
            var event = $.event.fix( arguments[0] || window.event );
            function delayed() {
                fn.call(scope, event);
                timeout = null;
            };
            if ( behavior === "debounce" && timeout )
                clearTimeout(timeout);
            if ( behavior === "throttle" && !timeout || behavior === "debounce" )
                timeout = setTimeout(delayed, delay);
        };
    };

    function actualHandler(event) {
        return $.event.handle.apply(this, arguments);
    };

})(jQuery);