
/**
 * use it to add text gradient effet on hover
 * usage : $(xxx).textGradient()
 * args : duration/ease/where
 */
$.fn.textGradient = function(args) {
	if(!RegExp("MSIE 8.0").test(navigator.userAgent) && !RegExp("MSIE 7.0").test(navigator.userAgent)) {
		var defaults = {
		'duration'  : 500,
		'ease':'linear',
		'where':'.title'
		};
		var o = $.extend(defaults,args);
		var element = this;
		var _getText = $($(this).find(o.where)).text();
		$($(element).find('.title')).empty();
		for (var w= 0,len = _getText.length; w < len; w++) {
			$('<span />').html(_getText[w]).attr({'data-letter':w,'class':'fx-gradfade'}).appendTo($($(element).find('.title')));
		}
		$(element).hover(
			function() {
				if(o.useActive) {
				if(!$(element).hasClass('active')) {
					$($(element).find('.fx-gradfade')).each(function(i) {
						$(this).stop().animate({'opacity':'1'},{duration:(o.duration*i),easing:o.ease});
					});
				}
				} else {
					$($(element).find('.fx-gradfade')).each(function(i) {
						$(this).stop().animate({'opacity':'1'},{duration:(o.duration*i),easing:o.ease});
					});
				}
			},
			function() {
				if(o.useActive) {
				if(!$(element).hasClass('active')) {
					$($(element).find('.fx-gradfade')).each(function(i) {
						$(this).stop().animate({'opacity':'0'},{duration:(o.duration*i),easing:o.ease});
					});
				}
				} else {
					$($(element).find('.fx-gradfade')).each(function(i) {
						$(this).stop().animate({'opacity':'0'},{duration:(o.duration*i),easing:o.ease});
					});
			}			
			}			
		)
	}
	return $(this); // retourne l'objet jquery et permet de conserver les appels enchainés'
}