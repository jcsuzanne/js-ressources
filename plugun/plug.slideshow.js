;(function(window,undefined) {
	$.fn.Slideshow = function(_options)  {
		var
			defaults = {
				step : 0,
				duration : 1000,
				interval : 5000,
				image : {
					width : 1280,
					height : 500
				}
			},
			_el = this,
			init = function()  {
				var
					$slideshow = $(_el),
					$slideshowImg = $(_el).find('img'),
					settings = $.extend({},defaults,_options),
					nbSlide = $slideshow.find('img').length,
					timer,
					//make the fullscreen
					fullscreen = function() {
						$slideshowImg.each(function(i) {
							$(this).attr('id','sf-'+i).addClass('sf-img').css('display','none');
							var FullscreenrOptions = {width: settings.image.width, height: settings.image.height, bgID: '#sf-'+i};
							$.fn.fullscreenr(FullscreenrOptions);
						});
					},
					//walk the slideshow
					walk = function() {
						if(nbSlide>1) {
							timer = setInterval(function() {
								settings.step++;
								if(settings.step>=nbSlide) settings.step = 0;
								$($slideshowImg.get(settings.step)).siblings().fadeOut(settings.duration);
								$($slideshowImg.get(settings.step)).fadeIn(settings.duration).css('display','block');
							},settings.interval);
						}
					},
					//sources are ready
					ready = function() {
						$('.preloading').remove();
						fullscreen();
						$slideshowImg.first().fadeIn(settings.duration);
						walk();
					},
					//preloader
					preloader = function() {
						$('<div />').attr({'class':'preloading'}).html('loading').appendTo($slideshow);
						var
							getSources = [],
							i = 0;
						$slideshowImg.each(function(i) {
							getSources.push($(this).attr('src'));
						});
						$.each(getSources,function(i,v){
							var img = $('<img />').attr('src', v).load(function(){
								i++;
								//all images are loaded
								if (nbSlide == i) {
									ready();
								}
							});
							//for IE
							if($.browser.msie && parseInt($.browser.version) < 9) img.trigger('load');
						});
					}();
			}
		init();
		return $(this);
	};
})(window);