;(function(window,undefined) {
	$.fn.zoomerDrag = function(_options,_callbackDrag) {
		var
			getOffsetPosition = function(obj) {
				var curleft=curtop=0;
				if (obj.offsetParent){
					curleft=obj.offsetLeft
					curtop=obj.offsetTop
					//if offset parent is the same object
					while(obj=obj.offsetParent){
						curleft+=obj.offsetLeft
						curtop+=obj.offsetTop
					}
				}
				return [curtop,curleft];
			},
			displayDraggable = function(_status) {
				if(_status) {
					//container
					$container.animate({'opacity':1});
				} else if(!_status) {
					//container
					$container.animate({'opacity':0});
				}
			},
			_el = this,
			$zoomer = $(_el),
			$handle = $(_el).find('.handle'),
			$container = $(_el).find('.container'),
			$zoomingImg = $container.find('.zoom'),
			defaults = {
				visual : '#product-main-visual',
				modifierY : 0,
				centerZooming : 150,
				adapt : false,
				mainContainerSize : 800,
				zoom : {
					duration:800,
					easing:Quint.easeInOut,
					complete:function() {
						if($(this).attr('data-status')=='close') {
							displayDraggable(false);
							isMousemoveActive = false;
						} else if($(this).attr('data-status')=='open') {
							$zoomingImg.stop().animate({'margin-top':'0'},{duration:1500,easing:'swing',complete:function() {
								isMousemoveActive = true;
							}});
						}
						//callback at the end of the tween
						if(typeof _callbackDrag == 'function') _callbackDrag();
					}
				}
			},
			isMousemoveActive = false,
			zoomHeight = 0,
			perimeter,
			isTablet=(RegExp("iPad").test(navigator.userAgent))?true:false,
			init = function() {
				var
				settings = $.extend({},defaults,_options),
				$visual = $(settings.visual),
				actions = function() {
					$handle.bind({
						'click':function() {
							if(!isTablet) {
								if($zoomer.attr('data-status')=='open' && isMousemoveActive) {
									$zoomer.stop().animate({'top':zoomHeight},settings.zoom);
									$zoomer.attr('data-status','close');
								}
							}
						}
					});
					$container.bind({
						'mousemove':function(e) {
							if(!isTablet) {
								if(isMousemoveActive) {
									var _offsetPos=getOffsetPosition(this);
									var _mouseCoords=(e.pageX-_offsetPos[1]);
									var _mouseCoordsY=(e.pageY-_offsetPos[0]);
									var _totalWidth = $(this).outerScrollWidth();
									var _totalHeight = $(this).outerScrollHeight();
									//get the pourcentage where i am with my mouse regarding the dimensions of the element
									//where i'm in middle, value = 0.5
									var _mousePercentX=_mouseCoords/$(this).width();if(_mousePercentX>1){_mousePercentX=1;}
									var _mousePercentY=_mouseCoordsY/$(this).height();if(_mousePercentY>1){_mousePercentY=1;}
									//get the distance we have between the start of the element and its end
									//by multiplicating it with the pourcentage we are by the mousemove, we know where to go
									//where im' in middle of a distance of 500, value = 250
									var _destX=Math.round(((_totalWidth-$(this).width())*(_mousePercentX)));
									var _destY=Math.round(((_totalHeight-$(this).height())*(_mousePercentY)));
									$(this).stop(true,false).animate({scrollLeft:_destX,scrollTop:_destY},{duration:800,easing:Circ.easeOut});
								}
							}
						}
					});
				}(),
				dimensionsHandle = function() {
					zoomHeight = parseInt($visual.height());
					if(settings.adapt) $zoomer.find('.zoom').addClass('adapted');
					$zoomer.css('top',zoomHeight);
				},
				drawEnlargeTouch = function() {
					$handle.bind({
						'click':function() {
							if($zoomer.attr('data-status')=='open') {
								$zoomer.stop().animate({'top':zoomHeight});
								$zoomer.attr('data-status','close');
								displayDraggable(false);
								$zoomingImg.css('opacity',0);
								$handle.find('.opened span').css('opacity',0);
							} else if($zoomer.attr('data-status')=='close') {
								$zoomer.stop().animate({'top': settings.modifierY});
								$zoomer.attr('data-status','open');
								displayDraggable(true);
								$zoomingImg.css('opacity',1);
							}
						}
					});
				},
				drawDraggable = function() {
					perimeter = [0,settings.modifierY,0,zoomHeight];
					$zoomer.draggable({axis:'y',containment:perimeter,cursor:'crosshair'});
					$zoomer.bind({
						"dragstart":function(e,ui) {
							displayDraggable(true);
						},
						"drag":function(e,ui) {
							$zoomingImg.css({'margin-top':ui.position.top*-1-settings.centerZooming});
						},
						"dragstop":function(e,ui) {
							if(ui.position.top>(parseInt($(window).height())-settings.modifierY)/2) {
								$(this).stop().animate({'top':zoomHeight},settings.zoom);
								$(this).attr('data-status','close');
							} else {
								$(this).stop().animate({'top': settings.modifierY},settings.zoom);
								$(this).attr('data-status','open');
							}
						}
					})
				}
				//ready
				$zoomer.closest('.main-container').css({'width': settings.mainContainerSize,'height': settings.mainContainerSize});
				dimensionsHandle();
				if(isTablet) drawEnlargeTouch(); else drawDraggable();
				$(window).resize(function() {
					dimensionsHandle();
				});
			}
		init();
		return $(this);
	}
})(window);