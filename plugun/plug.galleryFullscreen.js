;(function( window, undefined ) {
	$.fn.GalleryFullscreen = function(_options) {
		var
			getViewportSize = function(_element) {
				var size = [0, 0];
                var getElement=(typeof(_element) != 'undefined')? document.getElementById(_element.attr('id')):false;
                if(!getElement) {
                    if(typeof window.innerWidth != 'undefined') {
                        getElement = window;
                    } else if(typeof document.documentElement != 'undefined') {
                        getElement = document.documentElement;
                    } else {
                        getElement = document.getElementsByTagName('body')[0];
                    }
                }
				if (typeof getElement.innerWidth != 'undefined') size = [ getElement.innerWidth, getElement.innerHeight ];
				else if (typeof getElement != 'undefined' && typeof getElement.clientWidth != 'undefined' && getElement.clientWidth != 0)	size = [ getElement.clientWidth, getElement.clientHeight ];
				else size = [ getElement.clientWidth, getElement.clientHeight ];
				return size;
			},
			_el = this,
			viewport = getViewportSize(_el),
			defaults = {
                useLeftProperty : false,
                autoplay : false,
                interval : 2000,
				limitGap : 20,
                containerWidth : 16,
                containerHeight : 9,
				moveGap : viewport[0]/2-200,
				galleryFx : {
					duration:1000,
					easing:Expo.easeInOut
				},
				video : {
					features : ['playpause','progress','current','duration','tracks','volume','fullscreen']
				}
			},
			controllerVideo = {},
            statusSliding = false,
			isTablet=(RegExp("iPad").test(navigator.userAgent))?true:false,
			$listener=($('html').hasClass('ie8') || $('html').hasClass('ie7'))?$(document):$(window),
			ratio = viewport[0]/viewport[1],
		init = function() {
			var
				settings = $.extend({},defaults,_options),
				$gallery = $(_el),
				$btnPrevious = $(_el).find('.btn-previous'),
				$btnNext = $(_el).find('.btn-next'),
				galleryWidth = 0,
				nbSlides = $gallery.find('li').length-1,
				activeSlide = 0;

			//active touch
			if(isTablet) {
				$gallery.find('.nested,.nested ul').css('overflow','auto');
			}
			//display images
			$gallery.find('li').each(function(){
				$(this).css('width',viewport[0]);
				$(this).css('height',viewport[1]);
				//center horizontally
				if(ratio <= (settings.containerWidth/settings.containerHeight)){
					var thisImgWidth = viewport[1]*(settings.containerWidth/settings.containerHeight);
					var thisImgLeftMarg = (thisImgWidth)/2;
					if(isTablet) {
						$(this).find('img').css({'height':'100%','left':'0','margin-left':0,'margin-top':0,'top':'0px','width':'100%'});
					} else {
						$(this).find('img').css({'height':'100%','left':'50%','margin-left':-thisImgLeftMarg,'margin-top':0,'top':'0px','width':'auto'});
					}
				//center vertically
				}else{
					var thisImgHeight = (viewport[0]*settings.containerHeight)/settings.containerWidth;
					var thisImgTopMarg = (thisImgHeight)/2;
					$(this).find('img').css({'height':'auto','left':'0px','margin-left':0,'margin-top':-thisImgTopMarg,'top':'50%','width':'100%'});
				}
				galleryWidth += viewport[0];
			});
            if(settings.useLeftProperty) $gallery.find('.nested').css('width',galleryWidth)
			$gallery.find('ul').css('width',galleryWidth);
            $gallery.find('ul li').eq(0).addClass('active');
            var timerAutoplay;
            //counter
            var receptorCounter;
            if(settings.counter) {
                $receptorCounter    =   $gallery.find('.counter')
                for (var i = 0; i <= nbSlides; i++) {
                    $('<span />').html('&#9679;').appendTo($receptorCounter);
                }
                $gallery.find('.counter span').eq(activeSlide).addClass('active');
            }
            $gallery.attr('data-counter',activeSlide);
            $btnPrevious.addClass('cur-d');
            if(nbSlides==0) {
                $btnNext.addClass('cur-d');
            }

			//actions
			var actions = function() {

                if(settings.autoplay) {
                    var increment = 1;
                    timerAutoplay = setInterval(function() {
                       var goToSlide = increment%(nbSlides+1);
                        moveGallery('1',goToSlide);
                        increment++;
                    },settings.interval);
                }

				$btnPrevious.bind({
					'click':function(e) {
						e.preventDefault();
                        if(!statusSliding) {
                            activeSlide--;
                            if(activeSlide<=0) activeSlide = 0;
                            if(activeSlide != $gallery.attr('data-counter')) moveGallery('-1');
                            clearInterval(timerAutoplay);
                        }
					}
				});
				$btnNext.bind({
					'click':function(e) {
						e.preventDefault();
                        if(!statusSliding) {
                            activeSlide++;
                            if(activeSlide>=nbSlides) activeSlide = nbSlides;
                            if(activeSlide != $gallery.attr('data-counter')) moveGallery('1');
                            clearInterval(timerAutoplay);
                        }

					}
				});
				$('a[data-gallery]').bind({
					'click':function(e) {
						e.preventDefault();
                        if(!statusSliding) {
                            activeSlide=parseInt($(this).attr('data-gallery'))-1;
                            moveGallery('1',activeSlide);
                            clearInterval(timerAutoplay);
                        }
					}
				});
				$gallery.bind({
					'keydown':function(e) {
						switch(e.which) {
							case 37:
								$btnPrevious.triggerHandler('click');
								e.preventDefault();
								return false;
							break;
							case 39:
								$btnNext.triggerHandler('click');
								e.preventDefault();
								return false;
						}
					}
				});
				if(!isTablet) {
					$listener.bind({
						'mousemove':function(e) {
								if(e.pageX<settings.moveGap) {
									if(!$btnPrevious.hasClass('active')) {
										$btnPrevious.addClass('active');
										var _setOpa;
										if(activeSlide==0) _setOpa = 0; else _setOpa = 1;
//										$btnPrevious.stop(true,false).animate({'opacity':_setOpa},{complete:function() {
//										}});
									}
								} else if(e.pageX>$(window).width()-settings.moveGap) {
									if(!$btnNext.hasClass('active')) {
										$btnNext.addClass('active');
										if(activeSlide==nbSlides) _setOpa = 0; else _setOpa = 1;
//										$btnNext.stop(true,false).animate({'opacity':_setOpa},{complete:function() {
//										}});
									}
								} else {
									if(!$btnPrevious.hasClass('animated') || !$btnNext.hasClass('animated')) {
										$gallery.find('.controls ').addClass('animated');
//                                        $gallery.find('.controls ').css({'opacity':0});
                                        $gallery.find('.controls ').removeClass('animated').removeClass('active');;
                                        $gallery.find('.controls ').removeClass('animated');
									}
								}
							}
					});
				}

			}();
			//move gallery
			var moveGallery = function(_direction,_slide) {
				var _setScrollLeft;
                statusSliding = true;
                if(settings.useLeftProperty) settings.limitGap = 0;
				//move to slide
				if(typeof _slide !='undefined') {
					_setScrollLeft = viewport[0]*_slide;
                    activeSlide = _slide;
                    if(settings.useLeftProperty) _setScrollLeft *= -1;
					if(activeSlide==nbSlides) _setScrollLeft=_setScrollLeft+settings.limitGap;

				//move left/right
				} else {
					if(_direction=='1') {
                        if(settings.useLeftProperty) {
                            _setScrollLeft = parseInt($gallery.find('.nested').position().left) - viewport[0];
                        } else {
                            _setScrollLeft = parseInt($gallery.find('.nested').scrollLeft()) + viewport[0];
                        }
						if(activeSlide==nbSlides) _setScrollLeft=_setScrollLeft+settings.limitGap;
				//move left/right);
					} else {
                        if(settings.useLeftProperty) {
                            _setScrollLeft = parseInt($gallery.find('.nested').position().left) + viewport[0];
                        } else {
                            _setScrollLeft = parseInt($gallery.find('.nested').scrollLeft()) - viewport[0];
                        }
						if(activeSlide==0) _setScrollLeft = 0;
				//move left/right);
					}
				}

                if(settings.counter) {
                    $gallery.find('.counter span').removeClass('active');
                    $gallery.find('.counter span').eq(activeSlide).addClass('active');
                }

                if(settings.useLeftProperty) {
                    TweenLite.to($gallery.find('.nested'),(settings.galleryFx.duration/1000),{css:{transform:"translateX("+_setScrollLeft+"px)"},ease:settings.galleryFx.easing,
                        onComplete:function() {
                            statusSliding = false;
                    }});

                } else {
                    $gallery.find('.nested').stop().animate({scrollLeft:_setScrollLeft},settings.galleryFx);
                }
                //set active
                var $getSlides = $gallery.find('ul li');
                $getSlides.removeClass('active');
                setTimeout(function() {
                    $getSlides.eq(activeSlide).addClass('active');
                },settings.galleryFx.duration/2.2);
				//set buttons appearance
				$gallery.find('.controls ').removeClass('cur-p').removeClass('cur-d');
				if(isTablet) $gallery.find('.controls ').css({'opacity':1}).addClass('cur-p');
				if(activeSlide==0) {
//					$btnPrevious.css({'opacity':0}).addClass('cur-d');
					$btnPrevious.addClass('cur-d');
				} else if(activeSlide==nbSlides) {
//					$btnNext.css({'opacity':0}).addClass('cur-d');
					$btnNext.addClass('cur-d');
				} else {
//					$gallery.find('.controls ').css({'opacity':1}).addClass('cur-p');
//					$gallery.find('.controls ').addClass('cur-p');
					$gallery.find('.controls ').removeClass('cur-d');
				}
				//pause video
				$.each(controllerVideo.players,function(i,v) {
					v.pause();
				});
                $gallery.attr('data-counter',activeSlide);
			}
			var PlayerVideo = function() {
				controllerVideo.players = [];
				controllerVideo.params = {
					features: settings.video.features,
					videoWidth : viewport[0],
					videoHeight : viewport[1],
					defaultVideoWidth: viewport[0],
					defaultVideoHeight: viewport[1],
					pluginPath: "mediaelement/",
					success:function(mediaElement, domObject) {
						mediaElement.addEventListener('play', function(e) {
						}, false);
						mediaElement.addEventListener('pause', function(e) {
						}, false);
						mediaElement.addEventListener('ended', function(e) {
						}, false);
					}
				}
				$gallery.find('.media-video').each(function(i) {
					var _setID = 'player'+i;
					$(this).attr('id',_setID);
					var _setPlayer = new MediaElementPlayer('#player'+i,controllerVideo.params);
					if(Modernizr.video) {
						if($('.mejs-mediaelement').find('embed').length==0) $(this).attr('poster','');
					}
					controllerVideo.players.push(_setPlayer);
				});
			}();
		}
		/*return {
			'init':init
		}*/
		init();
		return $(this);
	};
	//window.GalleryFullscreen = GalleryFullscreen;
})(window);