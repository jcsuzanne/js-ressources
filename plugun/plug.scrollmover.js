/**
 * create a scrollmove context. Depending on the position of the mouse, the section moves
 * usage : $(xxx).scrollMover()
 * args : widthContainer/items/gap/lastChild
 */
$.fn.scrollMover = function(args) {
	var defaults = {
	    'widthContainer': 980,
	    'items' : '',
	    'gap' : 0,
	    'lastChild':':last-child'
	};
	var options = $.extend(defaults,args);
	var element = this;
	//Get our elements for faster access and set overlay width
	var container = element,items = $(options.items);var newPosLeft;
	container.scrollLeft(0);
	//Get menu width
	var containerWidth;
	if($("html").hasClass('ie7')) containerWidth = container.width(); else containerWidth = container.width()+7;
	//Remove scrollbars	
	container.css({overflow: 'hidden'});
	//Find last image container
	var lastItem = items.find(options.lastChild);
	//When user move mouse over menu
	container.mousemove(function(e){
		var getterItemsWidth = lastItem[0].offsetLeft + lastItem.outerWidth(true);
		var diff = Math.floor(($(document).width()-options.widthContainer)/2);
		var realPageX = Math.floor(e.pageX-diff)
		newPosLeft = (realPageX-options.gap) * ((getterItemsWidth-containerWidth)/(containerWidth-(options.gap*2)));
		if(realPageX<=containerWidth-options.gap) container.scrollLeft(newPosLeft);
	});
	return $(this); // retourne l'objet jquery et permet de conserver les appels enchainés'
}