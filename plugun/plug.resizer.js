/**
 * manage resize on a specified context
 * usage : $(xxx).resizer()
 * args : width/height/initialHeight
 */
$.fn.resizer = function(args) {
	var defaults = {
	    'width'  : 1280,
	    'height' : 500,
	    'initialHeight' : 500
	};
	var options = $.extend(defaults,args);
	var element = this;
	var doResize = function() {
		$(element).each(function() {
			//resize
			if($(window).width()/(options.width/options.height)>=options.initialHeight) {
				$(this).css({'width':$(window).width(),'height':$(window).width()/(options.width/options.height),'top':((($(window).width()/(options.width/options.height))-options.initialHeight)/2)*-1,'left':0});
			//pas de resize, centrage du visuel
			} else {
				$(this).css({'width':options.width,'height':options.initialHeight,'top':0,'left':((options.width-$(window).width())/2)*-1});
			}
		});
	}
	doResize();
	$(window).resize(doResize);
	return $(this); // retourne l'objet jquery et permet de conserver les appels enchainés'
}