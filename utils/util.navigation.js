;
(function(window,undefined) {
    Navigation = function() {

          //listen scrollbar limit
          //======================
          var listenScrollLimit = function(_$reference,_fnUP,_fnDOWN,_offsetBottom) {
            var $win = $(window),
            offsetBottom=(typeof _offsetBottom != 'undefined')?_offsetBottom:0;
            _$reference.bind({
                'scroll':function() {
                    if ($win.scrollTop() == 0)
                        if(_DEBUGG) console.log('listenScrollLimit top');
                        if(typeof _fnUP != 'undefined' && typeof _fnUP == 'function') _fnUP();
                    else if ($win.height() + $win.scrollTop() >= $(document).height() - offsetBottom) {
                        if(_DEBUGG) console.log('listenScrollLimit bottom');
                        if(typeof _fnDOWN != 'undefined' && typeof _fnDOWN == 'function') _fnDOWN();
                    }
                }
            })
          }


          //pull to refresh functionnality
          //example : http://interface.parismetropole2020.com/halle/pupitre_nuitblanche/maquette/interface.html
          //==============================
          var pullToRefresh = function(_reference,_fnLoadTop,_fnLoadBottom) {
               //capture events
               var
               hasTouch = 'ontouchstart' in window,
               ev = {};
               ev.click = hasTouch ? 'touchend' : 'click';
               ev.start = hasTouch ? 'touchstart' : 'mousedown';
               ev.move = hasTouch ? 'touchmove' : 'mousemove';
               ev.end = hasTouch ? 'touchend' : 'mouseup';
               //variables
               var
               $reference = $(_reference),
               $window = $(window),
               $pullTop = $('#pullTop'),
               $pullBottom = $('#pullBottom'),
               canLoad = false,
               loadDirection;
               //task
               $reference.on(ev.start, function(e) {
                    var event = e.originalEvent;
                    if(typeof event.touches != 'undefined') event = event.touches[0];
                    var startY = event.pageY;
                    $reference.on(ev.move, function(e) {
                         var event = e.originalEvent;
                         if(typeof event.touches != 'undefined') event = event.touches[0];
                         var endY = event.pageY;
                         var gapY = endY - startY;
                         if (gapY < 0) {
                              canLoad = false;
                         } else {
                              canLoad = true;
                         }
                         if (canLoad && parseInt($window.css('top')) == 0) {
                              if(gapY > 100) gapY = 100;
                              $pullTop.css({
                                   'height': gapY,
                                   'line-height':gapY+'px'
                              }).find('p').html('Relacher pour charger...');
                              loadDirection = 'top';
                              event.preventDefault();
                         } else if (parseInt($window.css('top')) <= $window.limitY[0] + 100) {
                              if(-gapY < 100 ) gapY = 100;
                              $pullBottom.css({
                                   'height': -gapY,
                                   'line-height':(-gapY)+'px'
                              }).find('p').html('Relacher pour charger...');
                              loadDirection = 'bottom';
                              event.preventDefault();
                         } else {
                              canLoad = false;
                         }
                    });
               });
               $reference.on(ev.end, function(e) {
                    if(canLoad) {
                         $pullTop.stop(true,false).animate({
                              'height': 50,
                              'line-height':50+'px'
                         }).html('');
                          $pullBottom.stop(true,false).animate({
                              'height': 50,
                              'line-height':50+'px'
                         }).html('');
                         if(typeof _fnLoadTop != 'undefined' && typeof _fnLoadTop == 'function' && loadDirection == 'top') _fnLoadTop();
                         if(typeof _fnLoadBottom != 'undefined' && typeof _fnLoadBottom == 'function' && loadDirection == 'bottom') _fnLoadBottom();
                    }
                    $reference.off(ev.move);
               });
               return this;
          }


          //wheel navigation by step
          //========================
          var wheelNav = function(_fnUP,_fnDOWN) {
            var handle = function(delta) {
                if (delta < 0) {
                    if(_DEBUGG) console.log('wheelNav down');
                    if(typeof _fnDOWN != 'undefined' && typeof _fnDOWN == 'function') _fnDOWN();
                } else {
                    if(_DEBUGG) console.log('wheelNav up');
                    if(typeof _fnUP != 'undefined' && typeof _fnUP == 'function') _fnUP();
                }
            }
            var wheel = function(event){
                var delta = 0;
                if (!event)
                    event = window.event;
                if (event.wheelDelta) {
                    delta = event.wheelDelta/120;
                } else if (event.detail) {
                    delta = -event.detail/3;
                }
                if (delta)
                    handle(delta);
                if (event.preventDefault)
                    event.preventDefault();
                event.returnValue = false;
            }
            var init = function() {
                if (window.addEventListener) window.addEventListener('DOMMouseScroll', wheel, false);
                window.onmousewheel = document.onmousewheel = wheel;
            }();
          }

        return {
            listenScrollLimit : listenScrollLimit,
            pullToRefresh : pullToRefresh,
            wheelNav : wheelNav
        }
    }()//ready to use
    //expose for window
    window.Navigation = Navigation;
})(window);