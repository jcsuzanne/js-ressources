;
(function(window,undefined) {
    DateManager = function() {

        var getDate = function() {
            return new Date();
        }

        return {
            getDate : getDate
        }
    }()//ready to use
    //expose for window
    window.DateManager = DateManager;
})(window);