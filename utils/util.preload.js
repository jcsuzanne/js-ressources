;(function(window,undefined) {
	Preloader = function() {

		//sources are ready*
          //=================
		var ready = function(_fnReady) {
			if(typeof _fnReady == 'function') _fnReady();
		};

		//preloader
          //=========
		var preloader = function(_getSources,_fnReady,_callbackFn,_fnError) {
			var
			i = 0,
               percent,
			nbImages = _getSources.length;
			$.each(_getSources,function(w,v){
				return $('<img />').attr('src', v)
                    .error(function() {
                         if(typeof _fnError != 'undefined' && typeof _fnError == 'function') _fnError();
                         if(_DEBUGG) alert('error on loading');
                    })
                    .load(function(){
					i++;
					//all images are loaded
					if (nbImages == i) {
						ready(_fnReady);
					}
                         percent = i/nbImages;
                         if(typeof _callbackFn != 'undefined' && typeof _callbackFn == 'function') _callbackFn(percent);
				});
				//for IE
//				if($.browser.msie && parseInt($.browser.version) < 9) img.trigger('load');
			});
		};

          //init the module
          //===============
		var initialize = function(_sources,_fnReady,_callback) {
			preloader(_sources,_fnReady,_callback);
		}

          //public methods
		return {
			'initialize':initialize
		}
	}()
	window.Preloader = Preloader;
})(window);


