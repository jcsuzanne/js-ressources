;(function(window,undefined) {

    // Utility for queue FX
    // How to?
    // =======
    // var $box1;
    // fx = function() {
    //$box1.queuedFx({queueName:"queue",delay:500},function() {
    //    $box1.find('.line').animate({'height':50},{duration:500,easing:'linear'});
    //});
    //$box1.queuedFx({queueName:"queue",delay:0},function() {
    //    $box1.find('.tray').addClass('active');
    //});
    //$box1.dequeue("queue");
    //}
    //fx();
    //=============================================================================
	$.fn.queuedFx = function(_options,fn)  {
		var
			defaults = {
				delay : 1000,
				queueName : 'namedQueue'
			},
			_el = this,
			init = function()  {
				var
					$reference = $(_el),
					settings = $.extend({},defaults,_options),
					queuedFonction = function() {
						$reference.queue(settings.queueName, function() {
						var
							self = this,
							$ref = $(self);
							if(typeof fn == 'function') fn();
							setTimeout(function() {
								$ref.dequeue(settings.queueName);
							}, settings.delay);
						});
					}()
			}
		init();
		return $(this);
	}

})(window);


