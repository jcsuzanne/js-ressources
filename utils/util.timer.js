;
(function(window,undefined) {
    TimerManager = function() {
        var circularGraph = function(_el,_countdown,_callback,_fill) {

            var
            timer,
            $reference = $(_el),
            timerSeconds = (typeof _countdown != 'undefined')?_countdown:10,
            timerFinish = new Date().getTime()+(timerSeconds*1000);

            //fill
            if(typeof _fill != 'undefined' && _fill == true) $reference.addClass('fill');

            var destroy = function(_instance) {
                _instance.instance.empty();
                clearInterval(_instance.instance.timer);
            }

            var draw = function(_percent) {
                $reference.html('<div class="percent"></div><div class="slice'+(_percent > 50?' gt50':'')+'"><div class="pie"></div>'+(_percent > 50?'<div class="pie fill"></div>':'')+'</div>');
                var deg = 360/100*_percent;
                $reference.find('.slice .pie').css({
                    '-moz-transform':'rotate('+deg+'deg)',
                    '-ms-transform':'rotate('+deg+'deg)',
                    '-webkit-transform':'rotate('+deg+'deg)',
                    '-o-transform':'rotate('+deg+'deg)',
                    'transform':'rotate('+deg+'deg)'
                });
                $reference.find('.percent').html(Math.round(_percent)+'%');
            }

            var stop = function() {
                var seconds = (timerFinish-(new Date().getTime()))/1000;
                //finish
                if(seconds <= 0){
                    draw(100);
                    clearInterval($reference.timer);
                    if(typeof _DEBUGG != 'undefined' && _DEBUGG == true) console.log('Finished counting down from '+timerSeconds);
                    if(typeof _callback != 'undefined' && typeof _callback == 'function') _callback();
                //redraw
                }else{
                    var percent = 100-((seconds/timerSeconds)*100);
                    draw(percent);
                }
            }

            var initialize = function() {
                if(typeof $reference.timer == 'undefined') $reference.timer = setInterval(stop,50);
                var returnInstance = {instance:$reference,events:this};
                return returnInstance;
            }

            return {
                destroy : destroy,
                initialize : initialize
            };
        }

        return {
            circularGraph : circularGraph
        }
    }()//ready to use
    //expose for window
    window.TimerManager = TimerManager;
})(window);
