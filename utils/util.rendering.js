;
(function(window,undefined) {
    Rendering = function() {

          //get the part of datas to load
          //render the html
          //and display it
          //@dependence : util.preload.js
          //@dependence : handlebars.js (http://handlebarsjs.com/)
          //=============================
          var dataToRender = function(_data,_template,_hasPhoto,_indexPhoto,_fnCallback,_fnNoData) {
               var
               displayFn=function() { displayContent(result); },
               callbackFn=(typeof _fnCallback != 'undefined')?_fnCallback:undefined,
               data = _data,
               sources = [],
               template = Handlebars.compile(_template),
               result = template(data);
               //preload
               $.each(data,function(i,v) {
                    sources.push(v[_indexPhoto]);
               });
               if(_hasPhoto && _data.length>0) {
                    Preloader.initialize(sources,displayFn,callbackFn);
               } else if(!_hasPhoto && _data.length>0) {
                    if(typeof displayFn != 'undefined' && typeof displayFn == 'function') displayFn();
               } else {
                    if(typeof _fnNoData != 'undefined' && typeof _fnNoData == 'function') _fnNoData();
               }
               return result;
          }

        return {
            dataToRender : dataToRender
        }
    }()//ready to use
    //expose for window
    window.Rendering = Rendering;
})(window);