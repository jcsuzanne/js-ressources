;
(function(window,undefined) {
    RequestManager = function() {

        //get data controller
        //===================
        var getDataAttribute = function(_el,_attr) {
             return $(_el).attr(_attr);
        }

        //get JSON
        //========
        var JSON = function(_url,_data) {
            var pData=(typeof _data != 'undefined')?_data:'';
            if(typeof _url != 'undefined') {
                return $.ajax({
                   url: _url,
                   dataType: 'json',
                   data : pData
                });
            }
        }

        // Get JSONP
        //
        //=========
        var JSONP = function(_url,_callback,_id) {
            if(typeof _url != 'undefined')
            {
                var
                s = document.createElement('script');
                s.src = _url+'&callback='+_callback;
                s.type = 'text/javascript';
                s.id=(typeof _id != 'undefined')?_id:'';
                document.body.appendChild(s);
            };

            var _callback = function(data) {
                console.log('what to do with '+data);
            };
        }

        //set the data controller
        //=======================
        var setDataController = function(_namespace,_el,_async) {
            if(typeof _namespace != 'undefined' && typeof _el != 'undefined') {
               var controller = getDataAttribute(_el,'data-controller');
               if(typeof _async != 'undefined' && _async == true) _namespace.tpl[controller].async(); else _namespace.tpl[controller].initialize();
            } else {
                if(typeof console != "undefined") console.log('error in setDataController');
            }
            if(typeof controller != 'undefined') return controller; else return false;
        }

        return {
            getDataAttribute : getDataAttribute,
            JSON : JSON,
            JSONP : JSONP,
            setDataController : setDataController
        }
    }()//ready to use
    //expose for window
    window.RequestManager = RequestManager;
})(window);