module.exports = function(grunt) {
    // Project configuration.
    //=======================
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        banner: '/*! <%= pkg.name %>  \n' +
                ' *\n' +
                ' * @version   v<%= pkg.version %>\n' +
                ' * @build     <%= grunt.template.today("dd/mm/yyyy HH:MM") %>\n' +
                ' */\n',
        jshint: {
            all: ['Gruntfile.js', 'assets/js/*.js']
        }
        ,
        sprite: {
            dist: {
                src:        ['assets/img/sprite/*.png'],
                destImg:    'assets/img/common/sprite.png',
                destCSS:    'assets/less/sprite.less',
                cssFormat:  'less',
                imgPath:    '../img/common/sprite.png',
                algorithm:  'left-right',
                cssFormat:  'less',
            }
        }
        ,
        recess: {
            dist: {
                options: {
                    compile: true
                },
                files: {
                    'assets/build/front.css': [
                        'assets/less/com.core.less',
                        'assets/less/com.skin.less',
                        'assets/less/adapt.all.less'
                    ]
                }
            }
        }
        ,
        concat: {
            options: {
                separator: ';'
            },
            css: {
                src: [
                       'assets/css/com.reset.css',
                       'assets/css/com.utils.css', 
                       'assets/css/lib.gcolumns.css',
                       'assets/css/com.animation.css'
                ],
                dest:  'assets/build/top.css'
            },
            jstop: {
                src: [
                       'assets/lib/modernizr/modernizr.custom.js'
                ],
                dest:  'assets/build/top.js'
            },
            js: {
                src: [
                       'assets/lib/json/json2.js',
                       'assets/lib/jquery/jquery-1.9.1.min.js',
                       'assets/lib/jquery-upgrade/jquery.enhance-1.1.js',
                       'assets/lib/jquery-upgrade/jquery.mousewheel.js',
                       'assets/lib/jquery-upgrade/jquery.outside.js',
                       
                       //for history html5 API
//                       'assets/lib/history/history.js',
//                       'assets/lib/history/history.adapter.jquery.js',
//                       'assets/lib/history/history.html4.js',


                       'assets/lib/kickstarter/kickstarter.js',
                       'assets/lib/gsap/TweenMax.min.js',
                       'assets/js/com.core.js',
                       'assets/js/com.toolbox.js',
                ],
                dest: 'assets/build/front.js'
            }
        }
        ,
        cssmin: {
            compress: {
                files: {
                    'assets/build/top.min.css': ['assets/build/top.css'],
                    'assets/build/front.min.css': ['assets/build/front.css']
                }
            }
        }
        ,
        uglify: {
            build: {
                files: {
                    'assets/build/top.min.js': ['assets/build/top.js'],
                    'assets/build/front.min.js': ['assets/build/front.js']
                }
            }
        }
        ,
        watch: {
            assets: {
                files: ['assets/less/*.less', 'assets/js/*.js'],
                tasks: [
                    'recess',
                    'concat',
                ]
            }
        }
    });

    // Load the plugins
    //=================
    grunt.loadNpmTasks('grunt-spritesmith');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-recess');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Default task(s).
    //=================
    grunt.registerTask('default',   ['recess','concat','cssmin','uglify']);
    grunt.registerTask('gensprite', ['sprite','recess','concat','cssmin','uglify']);
    grunt.registerTask('prod',      ['recess','concat','cssmin','uglify']);
    
    // Task watcher
    //=============
    grunt.registerTask('assets', [
        'watch:assets'
    ]);

};