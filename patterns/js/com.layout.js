;
(function(window,undefined) {

    JCS.layout = function() {

        var config = {
        };

        //main nav manager
        //================
        var mainnav = {
            fx : function() {
                 console.log('mainNav FX');
            }
        }

        //init the module
        //===============
        var initialize = function() {
            mainnav.fx();
            return this;
        }

        return {
            initialize : initialize
        }
    }()//ready to use
    //expose for window
    window.JCS.layout = JCS.layout;
})(window);