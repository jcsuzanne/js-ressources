// Variables
//==========

;
(function(window,undefined) {

     JCS.tpl.mySubClass = function() {

        var totoFOO = function() {
            alert('toto Foo');
        }

        var initialize = function() {
            totoFOO();
//            this.prototype.callbackTextLoading();
            this.prototype.functionOfMasterClass();
        }

        //public methods
        //==============
        return {
            initialize : initialize
        }
     }()//ready to use
     //expose for window
//     window.JCS.tpl.home = JCS.tpl.home;
JCS.tpl.mySubClass.prototype = JCS.tpl.home;
JCS.tpl.mySubClass.prototype.constructor = JCS.tpl.mySubClass;
})(window);

$(function() {
    JCS.tpl.mySubClass.initialize();
});